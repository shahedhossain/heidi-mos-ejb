package biz.shahed.freelance.heidi.mos.model.security;

import java.io.Serializable;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Version;

import biz.shahed.freelance.heidi.mos.model.embed.AuditChild;
import biz.shahed.freelance.heidi.mos.model.embed.UserAuth;
import biz.shahed.freelance.heidi.mos.model.embed.UserLog;

@Entity
@Table(name = "request_maps")
@Access(AccessType.FIELD)
public class Requestmap implements UserAuth, AuditChild, Serializable {

	private static final long serialVersionUID = -2918386768999435775L;

	@Id
	@TableGenerator(name = "request_maps", table = "sequences", pkColumnName = "f_seq_name", pkColumnValue = "request_maps", valueColumnName = "f_seq_value", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "request_maps")
	@Column(name = "f_requestmap_id")
	private Integer id;

	@Column(name = "f_requestmap_url", length = 100)
	private String requestmapUrl;

	@Column(name = "f_config_attribute", length = 60)
	private String configAttribute;
	
	@Embedded
	private UserLog log = new UserLog();

	@Version
	@Column(name = "f_revision", nullable = false)
	private Integer version;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getRequestmapUrl() {
		return requestmapUrl;
	}

	public void setRequestmapUrl(String requestmapUrl) {
		this.requestmapUrl = requestmapUrl;
	}

	public String getConfigAttribute() {
		return configAttribute;
	}

	public void setConfigAttribute(String configAttribute) {
		this.configAttribute = configAttribute;
	}

	@Override
	public UserLog getLog() {
		return log;
	}

	@Override
	public void setLog(UserLog log) {
		this.log = log;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	@Override
	public boolean hasChild() {
		return false;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (this.id != null ? this.id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {

		if (object == null || !(object instanceof Requestmap)) {
			return false;
		}

		Requestmap that = (Requestmap) object;
		if (this.id != null ? !this.id.equals(that.id) : that.id != null) {
			return false;
		}

		return true;
	}

	@Override
	public String toString() {
		return String.format("%s[%s=%s]", this.getClass().getName(), "id",
				this.id);
	}

}
