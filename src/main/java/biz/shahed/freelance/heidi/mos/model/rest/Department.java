package biz.shahed.freelance.heidi.mos.model.rest;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;

import biz.shahed.freelance.heidi.mos.model.embed.AuditChild;
import biz.shahed.freelance.heidi.mos.model.embed.UserAuth;
import biz.shahed.freelance.heidi.mos.model.embed.UserLog;

@Entity
@Table(name = "departments")
public class Department implements UserAuth, AuditChild, Serializable {

	private static final long serialVersionUID = -6599435895537995824L;

	@Id
	@Column(name = "f_department_id", unique = true, nullable = false)
	private Integer id;

	@Column(name = "f_name", nullable = false, length = 26)
	private String name;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "k_company_id", referencedColumnName = "f_company_id", nullable = false)
	private Company company;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinTable(name = "departments_workers", joinColumns = { @JoinColumn(name = "k_department_id", referencedColumnName = "f_department_id") }, inverseJoinColumns = { @JoinColumn(name = "k_worker_id", referencedColumnName = "f_worker_id") })
	private Set<Worker> workers = new HashSet<Worker>(0);

	@Embedded
	private UserLog log = new UserLog();

	@Version
	@Column(name = "f_revision", nullable = false)
	private Integer version;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public Set<Worker> getWorkers() {
		return workers;
	}

	public void setWorkers(Set<Worker> workers) {
		this.workers = workers;
	}

	public UserLog getLog() {
		return log;
	}

	public void setLog(UserLog log) {
		this.log = log;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	@Override
	public boolean hasChild() {
		return false;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (this.id != null ? this.id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {

		if (object == null || !(object instanceof Department)) {
			return false;
		}

		Department that = (Department) object;
		if (this.id != null ? !this.id.equals(that.id) : that.id != null) {
			return false;
		}

		return true;
	}

	@Override
	public String toString() {
		return String.format("%s[%s=%s]", this.getClass().getName(), "id",
				this.id);
	}

}