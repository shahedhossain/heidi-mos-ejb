package biz.shahed.freelance.heidi.mos.model.rest;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;

import biz.shahed.freelance.heidi.mos.model.embed.AuditChild;
import biz.shahed.freelance.heidi.mos.model.embed.UserAuth;
import biz.shahed.freelance.heidi.mos.model.embed.UserLog;

@Entity
@Table(name = "workers")
public class Worker implements UserAuth, AuditChild, Serializable {

	private static final long serialVersionUID = 912332980207942524L;

	@Id
	@Column(name = "f_worker_id", unique = true, nullable = false)
	private Integer id;

	@Column(name = "f_name", nullable = false, length = 26)
	private String name;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "k_office_id", referencedColumnName = "f_office_id", nullable = false)
	private Office office;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinTable(name = "departments_workers", joinColumns = { @JoinColumn(name = "k_worker_id", referencedColumnName = "f_worker_id") }, inverseJoinColumns = { @JoinColumn(name = "k_department_id", referencedColumnName = "f_department_id") })
	private Set<Department> departments = new HashSet<Department>(0);

	@Embedded
	private UserLog log = new UserLog();

	@Version
	@Column(name = "f_revision", nullable = false)
	private Integer version;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Office getOffice() {
		return office;
	}

	public void setOffice(Office office) {
		this.office = office;
	}

	public Set<Department> getDepartments() {
		return departments;
	}

	public void setDepartments(Set<Department> departments) {
		this.departments = departments;
	}

	public UserLog getLog() {
		return log;
	}

	public void setLog(UserLog log) {
		this.log = log;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	@Override
	public boolean hasChild() {
		return false;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (this.id != null ? this.id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {

		if (object == null || !(object instanceof Worker)) {
			return false;
		}

		Worker that = (Worker) object;
		if (this.id != null ? !this.id.equals(that.id) : that.id != null) {
			return false;
		}

		return true;
	}

	@Override
	public String toString() {
		return String.format("%s[%s=%s]", this.getClass().getName(), "id",
				this.id);
	}

}