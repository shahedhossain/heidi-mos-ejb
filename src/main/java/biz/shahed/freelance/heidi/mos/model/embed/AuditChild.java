package biz.shahed.freelance.heidi.mos.model.embed;

public interface AuditChild {

	public boolean hasChild();	
}
