package biz.shahed.freelance.heidi.mos.model.embed;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Embeddable
public class UserLog implements Serializable {

	private static final long serialVersionUID = 7633980225509154662L;

	@Column(name = "k_register_user")
	private Integer entryUser;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "f_register_date")
	private Date entryDate;

	@Column(name = "k_revision_user", insertable = false)
	private Integer modifyUser;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "f_revision_date", insertable = false)
	private Date modifyDate;

	public Integer getEntryUser() {
		return entryUser;
	}

	public void setEntryUser(Integer entryUser) {
		this.entryUser = entryUser;
	}

	public Date getEntryDate() {
		return entryDate;
	}

	public void setEntryDate(Date entryDate) {
		this.entryDate = entryDate;
	}

	public Integer getModifyUser() {
		return modifyUser;
	}

	public void setModifyUser(Integer modifyUser) {
		this.modifyUser = modifyUser;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

}
