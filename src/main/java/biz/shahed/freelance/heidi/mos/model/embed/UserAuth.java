package biz.shahed.freelance.heidi.mos.model.embed;

public interface UserAuth {

	public UserLog getLog();

	public void setLog(UserLog log);
}
