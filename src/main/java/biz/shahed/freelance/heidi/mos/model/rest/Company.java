package biz.shahed.freelance.heidi.mos.model.rest;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;

import biz.shahed.freelance.heidi.mos.model.embed.AuditChild;
import biz.shahed.freelance.heidi.mos.model.embed.UserAuth;
import biz.shahed.freelance.heidi.mos.model.embed.UserLog;

@Entity
@Table(name = "companies")
public class Company implements UserAuth, AuditChild, Serializable {

	private static final long serialVersionUID = -5626210466387216320L;

	@Id
	@Column(name = "f_company_id", unique = true, nullable = false)
	private Integer id;

	@Column(name = "f_name", nullable = false, length = 26)
	private String name;

	@OneToMany(mappedBy = "company", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Set<Office> offices = new HashSet<Office>();
	
	@OneToMany(mappedBy = "company", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Set<Department> departments = new HashSet<Department>();

	@Embedded
	private UserLog log = new UserLog();

	@Version
	@Column(name = "f_revision", nullable = false)
	private Integer version;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Office> getOffices() {
		return offices;
	}

	public void setOffices(Set<Office> offices) {
		this.offices = offices;
	}

	@Override
	public UserLog getLog() {
		return log;
	}

	@Override
	public void setLog(UserLog log) {
		this.log = log;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	@Override
	public boolean hasChild() {
		return false;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (this.id != null ? this.id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {

		if (object == null || !(object instanceof Company)) {
			return false;
		}

		Company that = (Company) object;
		if (this.id != null ? !this.id.equals(that.id) : that.id != null) {
			return false;
		}

		return true;
	}

	@Override
	public String toString() {
		return String.format("%s[%s=%s]", this.getClass().getName(), "id",
				this.id);
	}

}