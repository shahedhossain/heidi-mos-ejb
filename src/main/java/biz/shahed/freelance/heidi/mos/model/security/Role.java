package biz.shahed.freelance.heidi.mos.model.security;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Version;

import biz.shahed.freelance.heidi.mos.model.embed.AuditChild;
import biz.shahed.freelance.heidi.mos.model.embed.UserAuth;
import biz.shahed.freelance.heidi.mos.model.embed.UserLog;

@Entity
@Table(name = "roles")
@Access(AccessType.FIELD)
public class Role implements UserAuth, AuditChild, Serializable {

	private static final long serialVersionUID = -3007312720975226222L;

	@Id
	@TableGenerator(name = "roles", table = "sequences", pkColumnName = "f_seq_name", pkColumnValue = "roles", valueColumnName = "f_seq_value", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "roles")
	@Column(name = "f_role_id")
	private Integer id;

	@Column(name = "f_role", length = 60, nullable = false)
	private String name;

	@Column(name = "f_note", length = 100, nullable = true)
	private String note;

	@OneToMany(mappedBy = "role", fetch = FetchType.LAZY)
	private Set<User> users = new HashSet<User>(0);

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinTable(name = "authorities_roles", joinColumns = { @JoinColumn(name = "k_role_id", referencedColumnName = "f_role_id") }, inverseJoinColumns = { @JoinColumn(name = "k_authority_id", referencedColumnName = "f_authority_id") })
	private Set<Authority> authorities = new HashSet<Authority>(0);

	@Embedded
	private UserLog log = new UserLog();

	@Version
	@Column(name = "f_revision", nullable = false)
	private Integer version;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Set<User> getUsers() {
		return users;
	}

	public void setUsers(Set<User> users) {
		this.users = users;
	}

	public Set<Authority> getAuthorities() {
		return authorities;
	}

	public void setAuthorities(Set<Authority> authorities) {
		this.authorities = authorities;
	}

	@Override
	public UserLog getLog() {
		return log;
	}

	@Override
	public void setLog(UserLog log) {
		this.log = log;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	@Override
	public boolean hasChild() {
		return false;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (this.id != null ? this.id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {

		if (object == null || !(object instanceof Role)) {
			return false;
		}

		Role that = (Role) object;
		if (this.id != null ? !this.id.equals(that.id) : that.id != null) {
			return false;
		}

		return true;
	}

	@Override
	public String toString() {
		return String.format("%s[%s=%s]", this.getClass().getName(), "id",
				this.id);
	}

}