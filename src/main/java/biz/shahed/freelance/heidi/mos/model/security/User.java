package biz.shahed.freelance.heidi.mos.model.security;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import biz.shahed.freelance.heidi.mos.model.embed.AuditChild;
import biz.shahed.freelance.heidi.mos.model.embed.UserAuth;
import biz.shahed.freelance.heidi.mos.model.embed.UserLog;

@Entity
@Table(name = "users")
@Access(AccessType.FIELD)
@NamedQueries({ @NamedQuery(name = "User.findByUsername", query = "SELECT u FROM User u WHERE u.username = :username") })
public class User implements UserAuth, AuditChild, Serializable {

	private static final long serialVersionUID = -6158716575367939868L;

	@Id
	@TableGenerator(name = "users", table = "sequences", pkColumnName = "f_seq_name", pkColumnValue = "users", valueColumnName = "f_seq_value", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "users")
	@Column(name = "f_user_id")
	private Integer id;

	@Column(name = "f_username", length = 60)
	private String username;

	@Column(name = "f_password", length = 100)
	private String password;

	@Column(name = "f_account_active", columnDefinition = "bit", length = 1)
	private boolean accountActive;

	@Column(name = "f_account_unlocked", columnDefinition = "bit", length = 1)
	private boolean accountUnlocked;

	@Column(name = "f_account_unexpired", columnDefinition = "bit", length = 1)
	private boolean accountUnexpired;

	@Column(name = "f_password_unexpired", columnDefinition = "bit", length = 1)
	private boolean passwordUnexpired;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "f_account_unlock_date", nullable = true)
	private Date accountUnlockDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "f_account_expire_date", nullable = true)
	private Date accountExpireDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "f_password_expire_date", nullable = true)
	private Date passwordExpireDate;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "k_role_id", referencedColumnName = "f_role_id", nullable = false)
	private Role role;

	@Embedded
	private UserLog log = new UserLog();
	
	@Version
	@Column(name = "f_revision")
	private Integer version;	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isAccountActive() {
		return accountActive;
	}

	public void setAccountActive(boolean accountActive) {
		this.accountActive = accountActive;
	}

	public boolean isAccountUnlocked() {
		return accountUnlocked;
	}

	public void setAccountUnlocked(boolean accountUnlocked) {
		this.accountUnlocked = accountUnlocked;
	}

	public boolean isAccountUnexpired() {
		return accountUnexpired;
	}

	public void setAccountUnexpired(boolean accountUnexpired) {
		this.accountUnexpired = accountUnexpired;
	}

	public boolean isPasswordUnexpired() {
		return passwordUnexpired;
	}

	public void setPasswordUnexpired(boolean passwordUnexpired) {
		this.passwordUnexpired = passwordUnexpired;
	}

	public Date getAccountUnlockDate() {
		return accountUnlockDate;
	}

	public void setAccountUnlockDate(Date accountUnlockDate) {
		this.accountUnlockDate = accountUnlockDate;
	}

	public Date getAccountExpireDate() {
		return accountExpireDate;
	}

	public void setAccountExpireDate(Date accountExpireDate) {
		this.accountExpireDate = accountExpireDate;
	}

	public Date getPasswordExpireDate() {
		return passwordExpireDate;
	}

	public void setPasswordExpireDate(Date passwordExpireDate) {
		this.passwordExpireDate = passwordExpireDate;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	@Override
	public UserLog getLog() {
		return log;
	}

	@Override
	public void setLog(UserLog log) {
		this.log = log;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	@Override
	public boolean hasChild() {
		return false;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (this.id != null ? this.id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {

		if (object == null || !(object instanceof User)) {
			return false;
		}

		User that = (User) object;
		if (this.id != null ? !this.id.equals(that.id) : that.id != null) {
			return false;
		}

		return true;
	}

	@Override
	public String toString() {
		return String.format("%s[%s=%s]", this.getClass().getName(), "id",
				this.id);
	}

}